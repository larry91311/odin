using System;
// Remember to add the following using statemnt to the top of your class. This will give you access to all of Odin's attributes.
using Sirenix.OdinInspector;
using UnityEngine;
using System.Collections.Generic;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using System.Linq;

// public class MyCustomEditorWindow : OdinEditorWindow
// {
//     [MenuItem("My Game/My Editor")]
//     private static void OpenWindow()
//     {
//         GetWindow<MyCustomEditorWindow>().Show();
//     }

//     public string Hello;

//     [EnumToggleButtons, BoxGroup("Settings")]
//     public ScaleMode ScaleMode;

//     [FolderPath(RequireExistingPath = true), BoxGroup("Settings")]
//     public string OutputPath;

//     [HorizontalGroup(0.5f)]
//     public List<Texture> InputTextures;

//     [HorizontalGroup, InlineEditor(InlineEditorModes.LargePreview)]
//     public Texture Preview;

//     [Button(ButtonSizes.Gigantic), GUIColor(0, 1, 0)]
//     public void PerformSomeAction()
//     {

//     }

//     protected override void Initialize()
//     {
//         this.WindowPadding = Vector4.zero;
//     }

//     // protected override object GetTarget()
//     // {
//     //     return Selection.activeObject;
//     // }
// }

// public class MyCustomEditorWindow : OdinMenuEditorWindow
// {
//     [MenuItem("My Game/My Editor")]
//     private static void OpenWindow()
//     {
//         GetWindow<MyCustomEditorWindow>().Show();
//     }

//     protected override OdinMenuTree BuildMenuTree()
//     {
//         var tree = new OdinMenuTree();
//         tree.Selection.SupportsMultiSelect = false;

//         tree.Add("Settings", GeneralDrawerConfig.Instance);
//         tree.Add("Utilities", new TextureUtilityEditor());
//         tree.AddAllAssetsAtPath("Odin Settings", "Assets/Plugins/Sirenix", typeof(ScriptableObject), true, true);
//         return tree;
//     }
// }

// public class TextureUtilityEditor
// {
//     [BoxGroup("Tool"), HideLabel, EnumToggleButtons]
//     public Tool Tool;

//     public List<Texture> Textures;

//     [Button(ButtonSizes.Large), HideIf("Tool", Tool.Rotate)]
//     public void SomeAction() { }

//     [Button(ButtonSizes.Large), ShowIf("Tool", Tool.Rotate)]
//     public void SomeOtherAction() { }
// }


public class MyCustomEditorWindow : OdinMenuEditorWindow
{
    [MenuItem("My Game/My Editor")]
    private static void OpenWindow()
    {
        GetWindow<MyCustomEditorWindow>().Show();
    }

    protected override OdinMenuTree BuildMenuTree()
    {
        var tree = new OdinMenuTree();
        tree.DefaultMenuStyle = OdinMenuStyle.TreeViewStyle;

        tree.Add("Menu Style", tree.DefaultMenuStyle);

        var allAssets = AssetDatabase.GetAllAssetPaths()
            .Where(x => x.StartsWith("Assets/"))
            .OrderBy(x => x);

        foreach (var path in allAssets)
        {
            tree.AddAssetAtPath(path.Substring("Assets/".Length), path);
        }

        tree.EnumerateTree().AddThumbnailIcons();
        
        return tree;
    }
}
