#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Demos.FPSEditor
{
    public class ReadCodeSourceCode : FPSEquipableItem
    {
        [BoxGroup(STATS_BOX_GROUP)]
        public float BaseArmor;

        [BoxGroup(STATS_BOX_GROUP)]
        public float MovementSpeed;

        public override FPSItemTypes[] SupportedItemTypes
        {
            get
            {
                return new FPSItemTypes[]
                {
                    FPSItemTypes.Body,
                    FPSItemTypes.Head,
                    FPSItemTypes.Boots,
                    FPSItemTypes.OffHand
                };
            }
        }
    }
}
#endif
