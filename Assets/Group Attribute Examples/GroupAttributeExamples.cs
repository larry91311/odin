using System;
// Remember to add the following using statemnt to the top of your class. This will give you access to all of Odin's attributes.
using Sirenix.OdinInspector;
using UnityEngine;
using System.Collections.Generic;

public class GroupAttributeExamples : MonoBehaviour
{
    #region 組合屬性TabGroup(切頁Group)(左Tab標籤[TableGroup("左")]=屬於左Tab內容，右Tab標籤[TableGroup("右")]=屬於右Tab內容)
    [ TabGroup("First Tab")]   
    public int FirstTab;

    [ShowInInspector, TabGroup("First Tab")]   //ShowInInspector因為是屬性
    public int SecondTab { get; set; }

    [TabGroup("Second Tab")]
    public float FloatValue;

    [TabGroup("Second Tab"), Button]
    public void Button()
    {

    }
    #endregion

    #region 組合多Group
    [Button(ButtonSizes.Large)]
    [FoldoutGroup("Buttons in Boxes")]
    [HorizontalGroup("Buttons in Boxes/Horizontal", Width = 60)]
    [BoxGroup("Buttons in Boxes/Horizontal/One")]
    public void Button1() { }

    [Button(ButtonSizes.Large)]
    [BoxGroup("Buttons in Boxes/Horizontal/Two")]
    public void Button2() { }

    [Button]
    [BoxGroup("Buttons in Boxes/Horizontal/Double")]
    public void Accept() { }

    [Button]
    [BoxGroup("Buttons in Boxes/Horizontal/Double")]
    public void Cancel() { }
    #endregion

}
