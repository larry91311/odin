using System;
// Remember to add the following using statemnt to the top of your class. This will give you access to all of Odin's attributes.
using Sirenix.OdinInspector;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class Example
{
	[InfoBox(@"@""This member's parent property is called "" + $property.Parent.NiceName")]
    public string myStr;
}
public class AttributeExpressions : MonoBehaviour
{
    [InfoBox("@$value")]
    [InfoBox("@myStr")]    //同myStr
    [InfoBox(@"@""The current time is: "" + DateTime.Now.ToString(""HH:mm:ss"")")]    //同目前時間
    [ShowIf("@this.someNumber >= 0f && this.someNumber <= 10f")]
    public string myStr;

    public float someNumber;

    // Now, anywhere you declare it, myStr will now dynamically know the name of its parent
    public Example exampleInstance1;

    public List<string> someList;

    [OnValueChanged("@#(someList).State.Expanded = $value")]
    public bool expandList;

    void Start()
    {
        //myStr="1";
    }
}
