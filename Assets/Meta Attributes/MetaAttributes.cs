using System;
// Remember to add the following using statemnt to the top of your class. This will give you access to all of Odin's attributes.
using Sirenix.OdinInspector;
using UnityEngine;
using System.Collections.Generic;

public class MetaAttributes : MonoBehaviour
{
    #region Part1
    [ValidateInput("IsValid")]
    public int GreaterThanZero;

    private bool IsValid(int value)
    {
        return value > 0;
    }

    [OnValueChanged("UpdateRigidbodyReference")]
    public GameObject Prefab;

    public Rigidbody prefabRigidbody;

    private void UpdateRigidbodyReference()
    {
        if (this.Prefab != null)
        {
            this.prefabRigidbody = this.Prefab.GetComponent<Rigidbody>();
        }
        else
        {
            this.prefabRigidbody = null;
        }
    }
    #endregion

    #region Part2
    [Required]
    public GameObject RequiredReference;

    [InfoBox("This message is only shown when MyInt is even", "IsEven")]
    public int MyInt;

    private bool IsEven()
    {
        return this.MyInt % 2 == 0;
    }
    #endregion
}
