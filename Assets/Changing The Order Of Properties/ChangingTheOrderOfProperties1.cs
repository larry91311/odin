using System;
// Remember to add the following using statemnt to the top of your class. This will give you access to all of Odin's attributes.
using Sirenix.OdinInspector;
using UnityEngine;
using System.Collections.Generic;

public class ChangingTheOrderOfProperties1 : MonoBehaviour
{
    [PropertyOrder(2)]
    public int Second;

    [Button]
    [PropertyOrder(1)]
    public void First()
    {
        // ...
    }
}
