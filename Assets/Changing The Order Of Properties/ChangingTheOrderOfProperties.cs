using System;
// Remember to add the following using statemnt to the top of your class. This will give you access to all of Odin's attributes.
using Sirenix.OdinInspector;
using UnityEngine;
using System.Collections.Generic;

public class ChangingTheOrderOfProperties : MonoBehaviour
{
    [PropertyOrder(1)]
    public int Last;

    [PropertyOrder(-1)]
    public int First;

    // All properties default to 0.
    public int Middle;
}
