using System;
// Remember to add the following using statemnt to the top of your class. This will give you access to all of Odin's attributes.
using Sirenix.OdinInspector;
using UnityEngine;
using System.Collections.Generic;
public static class Guid
{
    public static int NewGuid()
    {
        return UnityEngine.Random.Range(0,10000);
    }
}
public class ExampleScript : MonoBehaviour
{
    
    
    #region FilePath 
	[FilePath(Extensions = ".unity")]
	public string ScenePath;
    #endregion
    #region Button
	[Button(ButtonSizes.Large)]
	public void SayHello()
	{
		Debug.Log("Hello button!");
	}
    #endregion
    #region HideInInspector、ShowInInspector
    [HideInInspector]
    public int NormallyVisible;

    [ShowInInspector]
    private bool normallyHidden;

    [ShowInInspector]
    public ScriptableObject Property { get; set; }
    #endregion
    #region 結合多屬性或變位置
    [PreviewField, Required, AssetsOnly]
    public GameObject Prefab;

    [HideLabel, Required, PropertyOrder(-5)]
    public string Name { get; set; }

    [Button(ButtonSizes.Medium), PropertyOrder(-3)]
    public void RandomName()
    {
        this.Name = Guid.NewGuid().ToString();
    }
    #endregion
    #region group attribute
    [HorizontalGroup("Split", Width = 50), HideLabel, PreviewField(50)]   //Split中的橫GROUP 第一個寫:置左 Field50
    public Texture2D Icon;

    [VerticalGroup("Split/Properties")]   //其他為Vertical 第二個寫:置右
    public string MinionName;

    [VerticalGroup("Split/Properties")]
    public float Health;

    [VerticalGroup("Split/Properties")]
    public float Damage;
    #endregion
    #region reference attribute、define attribute
    [LabelText("$IAmLabel")]
    public string IAmLabel;

    [ListDrawerSettings(
        CustomAddFunction = "CreateNewGUID",
        CustomRemoveIndexFunction = "RemoveGUID"
        )]
    public List<string> GuidList;

    private string CreateNewGUID()
    {
        return Guid.NewGuid().ToString();
    }

    private void RemoveGUID(int index)
    {
        this.GuidList.RemoveAt(index);
    }
    #endregion
}
