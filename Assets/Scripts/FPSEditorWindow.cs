#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Demos.FPSEditor
{
    using Sirenix.OdinInspector.Editor;
    using Sirenix.Utilities;
    using Sirenix.Utilities.Editor;
    using UnityEditor;
    using UnityEngine;
    using System.Linq;

    public class FPSEditorWindow : OdinMenuEditorWindow
    {
        [MenuItem("Tools/Odin Inspector/Demos/FPS Editor")]
        private static void Open()
        {
            var window = GetWindow<FPSEditorWindow>();
            window.position = GUIHelper.GetEditorWindowRect().AlignCenter(800, 500);
        }

        protected override OdinMenuTree BuildMenuTree()
        {
            var tree = new OdinMenuTree(true);
            tree.DefaultMenuStyle.IconSize = 28.00f;
            tree.Config.DrawSearchToolbar = true;

            // Adds the character overview table.
            FPSCharacterOverview.Instance.UpdateCharacterOverview();
            tree.Add("Characters", new FPSCharacterTable(FPSCharacterOverview.Instance.AllCharacters));

            //Adds all characters.
            tree.AddAllAssetsAtPath("Characters", "Scripts/FPSCharacterNeed/FPSCharacters", typeof(FPSCharacter), true, true);

            // Add all scriptable object items.
            tree.AddAllAssetsAtPath("", "Scripts/FPSCharacterNeed/FPSItems", typeof(FPSItem), true)
                .ForEach(this.AddDragHandles);

            // // Add drag handles to items, so they can be easily dragged into the inventory if characters etc...
            // tree.EnumerateTree().Where(x => x.Value as Item).ForEach(AddDragHandles);

            // // Add icons to characters and items.
            // tree.EnumerateTree().AddIcons<Character>(x => x.Icon);
            // tree.EnumerateTree().AddIcons<Item>(x => x.Icon);

            return tree;
        }

        private void AddDragHandles(OdinMenuItem menuItem)
        {
            menuItem.OnDrawItem += x => DragAndDropUtilities.DragZone(menuItem.Rect, menuItem.Value, false, false);
        }
    }


}
#endif
