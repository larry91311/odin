#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Demos.FPSEditor
{
    using System;

    [Serializable]
    public struct FPSItemSlot
    {
        public int ItemCount;
        public FPSItem Item;
    }
}
#endif
