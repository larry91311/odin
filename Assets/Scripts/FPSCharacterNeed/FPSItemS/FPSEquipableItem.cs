#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Demos.FPSEditor
{
    public abstract class FPSEquipableItem : FPSItem
    {
        [BoxGroup(STATS_BOX_GROUP)]
        public float Durability;

        [VerticalGroup(LEFT_VERTICAL_GROUP + "/Modifiers")]
        public FPSStatList Modifiers;
    }
}
#endif
