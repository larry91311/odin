#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Demos.FPSEditor
{
    public enum FPSCharacterAlignment
    {
        LawfulGood,
        NeutralGood,
        ChaoticGood,
        LawfulNeutral,
        TrueNeutral,
        ChaoticNeutral,
        LawfulEvil,
        NeutralEvil,
        ChaoticEvil,
    }
}
#endif
