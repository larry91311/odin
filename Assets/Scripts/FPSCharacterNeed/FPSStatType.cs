#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Demos.FPSEditor
{
    public enum FPSStatType
    {
        Shooting,
        Melee,
        Social,
        Animals,
        Medicine,
        Cooking,
        Mining,
        Crafting
    }
}
#endif
