#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Demos.FPSEditor
{
    using System;
    using UnityEngine;

    // 
    // CharacterStats is simply a StatList, that expose the relevant stats for a character.
    // Also note that the StatList might look like a dictionary, in how it's used, 
    // but it's actually just a regular list, serialized by Unity. Take a look at the StatList to learn more.
    // 

    [Serializable]
    public class FPSCharacterStats
    {
        [HideInInspector]
        public FPSStatList Stats = new FPSStatList();

        [ProgressBar(0, 100), ShowInInspector]
        public float Shooting
        {
            get { return this.Stats[FPSStatType.Shooting]; }
            set { this.Stats[FPSStatType.Shooting] = value; }
        }

        [ProgressBar(0, 100), ShowInInspector]
        public float Melee
        {
            get { return this.Stats[FPSStatType.Melee]; }
            set { this.Stats[FPSStatType.Melee] = value; }
        }

        [ProgressBar(0, 100), ShowInInspector]
        public float Social
        {
            get { return this.Stats[FPSStatType.Social]; }
            set { this.Stats[FPSStatType.Social] = value; }
        }

        [ProgressBar(0, 100), ShowInInspector]
        public float Animals
        {
            get { return this.Stats[FPSStatType.Animals]; }
            set { this.Stats[FPSStatType.Animals] = value; }
        }

        [ProgressBar(0, 100), ShowInInspector]
        public float Medicine
        {
            get { return this.Stats[FPSStatType.Medicine]; }
            set { this.Stats[FPSStatType.Medicine] = value; }
        }

        [ProgressBar(0, 100), ShowInInspector]
        public float Crafting
        {
            get { return this.Stats[FPSStatType.Crafting]; }
            set { this.Stats[FPSStatType.Crafting] = value; }
        }
    }
}
#endif

