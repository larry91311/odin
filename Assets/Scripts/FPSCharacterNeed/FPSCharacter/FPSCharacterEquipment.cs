#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Demos.FPSEditor
{
    using System;

    [Serializable]
    public class FPSCharacterEquipment
    {
        [ValidateInput("IsMainHand")]
        public FPSEquipableItem MainHand;

        [ValidateInput("IsOffHand")]
        public FPSEquipableItem Offhand;

        [ValidateInput("IsHead")]
        public FPSEquipableItem Head;

        [ValidateInput("IsBody")]
        public FPSEquipableItem Body;

#if UNITY_EDITOR
        private bool IsBody(FPSEquipableItem value)
        {
            return value == null || value.Type == FPSItemTypes.Body;
        }

        private bool IsHead(FPSEquipableItem value)
        {
            return value == null || value.Type == FPSItemTypes.Head;
        }

        private bool IsMainHand(FPSEquipableItem value)
        {
            return value == null || value.Type == FPSItemTypes.MainHand;
        }

        private bool IsOffHand(FPSEquipableItem value)
        {
            return value == null || value.Type == FPSItemTypes.OffHand;
        }
#endif
    }
}
#endif
