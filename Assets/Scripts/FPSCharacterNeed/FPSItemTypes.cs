#if UNITY_EDITOR

namespace Sirenix.OdinInspector.Demos.FPSEditor
{
    public enum FPSItemTypes
    {
        None,
        MainHand,
        OffHand,
        Body,
        Head,
        Ring,
        Amulet,
        Boots,
        Consumable,
        Flask
    }
}
#endif
