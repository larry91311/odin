using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Demos;
using System.IO;

public class TestTextAsset : MonoBehaviour
{
    public static Object textAssetObject;

    public static List<string> lineList = new List<string>();

    [Button]
    public void OpenReadCodeWindow()
    {
        // //清空每行List Buffer
        // lineList.Clear();
        // string line = string.Empty;

        // string address = "Assets/Resources/ReadCodeTxT.txt";

        // Debug.Log(System.IO.File.Exists(address));

        // //讀取樣板檔案每行，添加REPLACE後的行進LINELIST
        // using (StreamReader sr = new StreamReader(address))
        // {
        //     lineList.Add(line);
        // }

        ReadCodeWindow.OpenWindow();
    }
}
