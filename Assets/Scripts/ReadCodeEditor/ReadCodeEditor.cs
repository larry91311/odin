using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Sirenix.OdinInspector.Demos;

namespace EditorTool.Utility
{
    public class ReadCodeEditor : EditorWindow
    {
        private string originText = "";
        private string changeToText = "";
        public static List<string> lineList = new List<string>();


        [MenuItem("Project/ReadCodeEditor")]
        static ReadCodeEditor GetWindow()
        {
            var window = EditorWindow.GetWindow<ReadCodeEditor>();
            return window;
        }


        private void OnGUI()
        {
            //EditorGUILayout.LabelField( "originChar" );
            originText = EditorGUI.TextField(new Rect(10, 25, position.width - 20, 20),
                "originChar:",
                originText);
            //EditorGUILayout.LabelField( "changeToChar" );
            changeToText = EditorGUI.TextField(new Rect(10, 45, position.width - 20, 20),
                "changeToChar:",
                changeToText);
            if (GUILayout.Button("ChangeChar"))
            {
                foreach (/*UnityEditor.AddressableAssets.Settings.AddressableAssetGroup*/var obj in Selection.objects)
                {
                    //Debug.Log(AssetDatabase.GetAssetPath(obj))   ;
                    ReadConfig(AssetDatabase.GetAssetPath(obj));
                }
            }



            //

        }
        string[] clipName = { "", "", "" };

        public void ReadConfig(string address)
        {
            // if (originText.Length == 0 || changeToText.Length == 0)    //阻擋超其一為空 //危險替換:其中一方為空字串的替換，等於 空換成字元=未知，或者 字元換成空=字元消失，間隔消失
            // {
            //     Debug.Log("沒輸入到");
            //     return;
            // }
            // if (originText.Length > 1 || changeToText.Length > 1)      //阻擋超過一位元
            // {
            //     Debug.Log("輸入超過一CHAR");
            //     return;
            // }


            lineList.Clear();    //清空LINEBUFFER

            // Read and show each line from the file.
            string line = "";    //初始讀寫LINE STRING


            //讀取每行，添加REPLACE後的行進LINELIST
            using (StreamReader sr = new StreamReader(address))
            //using (StreamReader sr = new StreamReader(AssetDatabase.GetAssetPath(GameObject.Find("AssetList").GetComponent<NewBehaviourScript>().objectToLoad))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    lineList.Add(line);
                }
            }

            Debug.Log(lineList.Count);
            ReadCodeWindow.OpenWindow();

            // //寫入每行，每行變更為LINELIST內的替換行
            // //using (StreamWriter sw = new StreamWriter(Application.streamingAssetsPath+"/Local_Hotfix.asset"))
            // using (StreamWriter sw = new StreamWriter(address))
            // {
            //     foreach (string eachLine in lineList)
            //     {
            //         sw.WriteLine(eachLine);
            //     }
            // }
        }

        // public void ReadFile()
        // {
        //     // Get the directories currently on the C drive.
        //     DirectoryInfo[] cDirs = new DirectoryInfo(@"c:\").GetDirectories();

        //     // // Write each directory name to a file.
        //     // using (StreamWriter sw = new StreamWriter("CDriveDirs.txt"))
        //     // {
        //     //     foreach (DirectoryInfo dir in cDirs)
        //     //     {
        //     //         sw.WriteLine(dir.Name);
        //     //     }
        //     // }

        //     // Read and show each line from the file.
        //     string line = "";
        //     using (StreamReader sr = new StreamReader("CDriveDirs.txt"))
        //     {
        //         while ((line = sr.ReadLine()) != null)
        //         {
        //             Debug.Log(line);
        //         }
        //     }
        // }

    }
}
