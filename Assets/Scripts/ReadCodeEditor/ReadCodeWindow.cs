#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Demos
{
    using Sirenix.OdinInspector.Editor;
    using System.Linq;
    using UnityEngine;
    using Sirenix.Utilities.Editor;
    using Sirenix.Serialization;
    using UnityEditor;
    using Sirenix.Utilities;
    using System.IO;
    using System.Collections.Generic;
    using EditorTool.Utility;

    public class ReadCodeWindow : OdinMenuEditorWindow
    {
        #region Variables
        /// <summary>
        /// 每行List
        /// </summary>
        /// <typeparam name="string"></typeparam>
        /// <returns></returns>
        private List<string> lineList = new List<string>();

        /// <summary>
        /// Menu分類Title
        /// </summary>
        /// <typeparam name="string"></typeparam>
        /// <returns></returns>
        private List<string> menuTitleList = new List<string>()
        {
            "NameSpace",
            "Using",
            "Class",
            "Variables",
            "Public Methods",
            "Private Methods"
        };
        #endregion

        [MenuItem("Tools/CustomEditor/ReadCode")]
        public static void OpenWindow()
        {
            var window = GetWindow<ReadCodeWindow>();
            window.position = GUIHelper.GetEditorWindowRect().AlignCenter(800, 600);


        }

        protected override OdinMenuTree BuildMenuTree()
        {
            //建立Tree
            OdinMenuTree tree = new OdinMenuTree(supportsMultiSelect: true);

            //建立Tree左側列Menu，依據menuTitleList
            foreach (var eachLine in menuTitleList)
            {
                tree.Add(eachLine, new GUIContent());
            }

            //邏輯區
            //初始化
            string line = string.Empty;             //單行讀取器
            string address = string.Empty;          //點選地址
            lineList.Clear();                       //清空所有行List

            //選擇物件
            foreach (var obj in Selection.objects)
            {
                address = AssetDatabase.GetAssetPath(obj);
            }

            //讀取每行
            using (StreamReader sr = new StreamReader(address))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    lineList.Add(line);
                    LineProcessor(tree, line);
                }
            }





            return tree;
        }

        #region Public Methods
        #endregion

        #region Private Methods
        void LineProcessor(OdinMenuTree tree, string line)
        {
            CheckNamespaceProcessor(tree, line);
            CheckUsingProcessor(tree, line);
            CheckClassProcessor(tree, line);
        }

        void CheckNamespaceProcessor(OdinMenuTree tree, string line)
        {
            if (line.Contains("namespace"))
            {
                string secondStringAfterSplitEmpty = line.Split(" ")[1];
                tree.Add("NameSpace/" + secondStringAfterSplitEmpty, new GUIContent(line));
            }
        }

        void CheckUsingProcessor(OdinMenuTree tree, string line)
        {
            if (line.Contains("using"))
            {
                string secondStringAfterSplitEmpty = line.Split(" ")[1];
                tree.Add("Using/" + secondStringAfterSplitEmpty, new GUIContent(line));
            }
        }

        void CheckClassProcessor(OdinMenuTree tree, string line)
        {
            if (line.Contains("class"))
            {
                string thirdStringAfterSplitEmpty = line.Split(" ")[2];
                tree.Add("Class/" + thirdStringAfterSplitEmpty, new GUIContent(line));
            }
        }
        #endregion
    }
}
#endif