using System;
// Remember to add the following using statemnt to the top of your class. This will give you access to all of Odin's attributes.
using Sirenix.OdinInspector;
using UnityEngine;
using System.Collections.Generic;

public class PropertyStates : MonoBehaviour
{
    // It is generally recommended to use the OnStateUpdate attribute to control the state of properties
    [OnStateUpdate("@#(exampleList).State.Expanded = $value.HasFlag(ExampleEnum.UseStringList)")]
    public ExampleEnum exampleEnum;

    public List<string> exampleList;

    [Flags]
    public enum ExampleEnum
    {
        None,
        UseStringList = 1 << 0,
        // ...
    }


    // All groups silently have "#" prepended to their path identifier to avoid naming conflicts with members.
    // Thus, the "Tabs" group is accessed via the "#(#Tabs)" syntax.
    [OnStateUpdate("@#(#Tabs).State.Set<int>(\"CurrentTabIndex\", $value + 1)")]
    [PropertyRange(1, "@#(#Tabs).State.Get<int>(\"TabCount\")")]
    public int selectedTab = 1;

    [TabGroup("Tabs", "Tab 1")]
    public string exampleString1;

    [TabGroup("Tabs", "Tab 2")]
    public string exampleString2;

    [TabGroup("Tabs", "Tab 3")]
    public string exampleString3;
}
